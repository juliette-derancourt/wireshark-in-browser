FROM debian:stable-slim

ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_PRIORITY critical
ENV DEBCONF_NOWARNINGS yes

# Broadway setup
ENV GDK_BACKEND broadway
ENV BROADWAY_DISPLAY :5

EXPOSE 8085

RUN apt-get update -q -q
RUN apt-get upgrade --yes --force-yes

# Install GTK
RUN apt-get -y --no-install-recommends install libgtk-3-bin

# Install wireshark
RUN apt-get -y --no-install-recommends install wireshark-gtk

# Copy the trace to open in wireshark
COPY trace-example.pcapng /trace-example.pcapng

# Open wireshark with broadway display in the browser
COPY init.sh /init.sh
CMD ["/init.sh"]
