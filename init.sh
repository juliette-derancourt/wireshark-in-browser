#!/usr/bin/env bash

set -x

# Launch Broadway
nohup broadwayd :5 &

# Open the trace in wireshark
wireshark-gtk /trace-example.pcapng
