#!/bin/bash

docker build -t wireshark-broadway .

docker run -d --rm -it -p 8085:8085 --name wireshark-broadway wireshark-broadway
